#ifndef LAB_H
#define LAB_H

#include <QObject>
#include <QVector>

class Lab : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(double possib READ possib WRITE setPossib NOTIFY possibChanged)
    Q_PROPERTY(int steps READ steps WRITE setSteps NOTIFY stepsChanged)

    int m_height;
    int m_width;
    double m_possib;
    int m_steps;
    QVector<QVector<int>> matrixInt;

public:
    explicit Lab(QObject *parent = nullptr);

    Q_INVOKABLE int convertToVector(QString str, double possibil);
    Q_INVOKABLE double xoring(QVector<int> unknown, QVector<int> matrix, double p);
    Q_INVOKABLE int process(QString str);
    Q_INVOKABLE void clear();
    Q_INVOKABLE int forZero(QVector<int> unknown, QVector<int> matrix);
    Q_INVOKABLE int forOne(QVector<int> unknown, QVector<int> matrix);

    int height() const
    {
        return m_height;
    }

    int width() const
    {
        return m_width;
    }

    double possib() const
    {
        return m_possib;
    }
    int steps() const
    {
        return m_steps;
    }

signals:

    void heightChanged(int height);

    void widthChanged(int width);

    void possibChanged(double possib);

    void stepsChanged(double possib);

public slots:
    void setSteps(int steps)
    {
        if (m_steps == steps)
            return;

        m_steps = steps;
        emit stepsChanged(m_steps);
    }
    void setHeight(int height)
    {
        if (m_height == height)
            return;

        m_height = height;
        emit heightChanged(m_height);
    }
    void setWidth(int width)
    {
        if (m_width == width)
            return;

        m_width = width;
        emit widthChanged(m_width);
    }

    void setPossib(double possib)
    {
        if (m_possib == possib)
            return;

        m_possib = possib;
        emit possibChanged(m_possib);
    }
};

#endif // LAB_H
