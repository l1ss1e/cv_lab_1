    #include "lab.h"
#include <QDebug>

Lab::Lab(QObject *parent) : QObject(parent)
{

}

int Lab::convertToVector(QString str, double possibil)
{
    int absoluteResult = 0;
    double r, r2;
    //qDebug() << "Matrica =" << str;
    QStringList usableMatrix = str.split('\n');
    QVector<int> usableVector;
    QList<double> results;
    for (int i = 1; i!= usableMatrix.length(); ++i) // we start from 1 to avoid first number in matrix
    {
        QStringList use = usableMatrix[i].split(" ");
        for(QString n:use)
            usableVector.push_back(n.toInt());
    }

    qDebug() << "start xoring";

    for(auto m : matrixInt)
    {
        //qDebug() << m << ": "<< xoring(usableVector, m, possibil);
        results.append(xoring(usableVector, m, possibil));
    }
     qDebug() << "results =" << results;

     r = *std::min_element(results.begin(), results.end());
     r2 = *std::max_element(results.begin(), results.end());

     if (possibil < 0.5)
     {
         //qDebug() << "ddddddd"<<r<<"\nRESULT IS: "<<results.indexOf(r);
         absoluteResult = results.indexOf(r);
     }
     if (possibil >= 0.5)
     {
         //qDebug() << "ddddddd"<<r2<<"\nRESULT IS: "<<results.indexOf(r2);
         absoluteResult = results.indexOf(r2);
     }
     if (possibil == 0.0) {
         for(auto m : matrixInt)
         {
          int a = 0;
          a = forZero(usableVector, m);
          if (a == 0) absoluteResult = matrixInt.indexOf(m) ;
         }
     }
     if (possibil == 1.0) {
         QList<int> r;
         for(auto m : matrixInt)
         {
             r.append(forOne(usableVector, m));
         }
         int r2 = *std::max_element(r.begin(), r.end());
         absoluteResult = r.indexOf(r2);

     }
     qDebug() << "RESULT IS" << absoluteResult;
     return absoluteResult;
    //qDebug() << "xoring =" << xoring(usableVector, matrixInt[1], 1);
}

double Lab::xoring(QVector<int> unknown, QVector<int> matrix, double p)
{
qDebug() << unknown.length() << matrix.length();
    double sum = 0;
    for(int i=0; i<unknown.length(); i++)
    {
        sum = sum + ((unknown[i]) ^ (matrix[i]))*(unknown.length()*p+ (1^unknown[i] ^ matrix[i]))*unknown.length()*(1-p);
        //qDebug() << "(" << (unknown[i])<<"^"<<(matrix[i])<<")*("<<unknown.length()<<"*"<<p<<"+(1^"<<unknown[i]<<"^"<<matrix[i]<<"))*"<<unknown.length()<<"*"<<(1-p)<<"="<<sum;;

    }

    return sum;
}

int Lab::process(QString str)//size = width*height
{
    QStringList usableMatrix = str.split('\n');

    QVector<QString> matrix;
    matrix.resize(10);

    int m = -1;
    for (int i = 0; i < usableMatrix.length(); i++)
    {
        QString str = usableMatrix[i];

        if (str.length() == 1) {
            m++;
            continue;
        }

        matrix[m] += str + " ";
//            if (usableMatrix[i] == '8') { //если єто 0
//                for (int j = i+1; j<=i+height; j++)//присваеваем все что после 0 до конца матриці цифрі 0 в новую матрицу о
//                    e.append(usableMatrix[j]);
//            }
//            if (usableMatrix[i] == '9') { //если єто 0
//                for (int j = i+1; j<=i+height; j++)//присваеваем все что после 0 до конца матриці цифрі 0 в новую матрицу о
//                    nine.append(usableMatrix[j]);
//            }
//        }
//        //xor0 = xoring(usableMatrix)

    }

    for(QString m : matrix) {
        QStringList mat = m.trimmed().split(" ");

        QVector<int> vec;
        for(QString ma : mat)
            vec << ma.toInt();
        matrixInt << vec;
    }
//    qDebug() << "ommmmm" << height;
//    qDebug() <<"dddddd" <<z;
//    qDebug() << "jnjjjjjj" << usableMatrix;
    return 1;

}

void Lab::clear()
{
    // matrixInt.clear();
}

int Lab::forZero(QVector<int> unknown, QVector<int> matrix)
{
    int sum=0;
    for(int i=0; i<unknown.length(); i++)
    {
        sum = sum + unknown[i]^matrix[i];
    }

    return sum;
}

int Lab::forOne(QVector<int> unknown, QVector<int> matrix)
{
    int sum=0;
    for(int i=0; i<unknown.length(); i++)
    {
        sum = sum + unknown[i]^matrix[i];
    }

    return sum;
}
