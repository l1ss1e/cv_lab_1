import QtQuick 2.12
import QtQuick.Controls 2.5
import QtWebSockets 1.1
import QtQuick.Layouts 1.12
import Lab 1.0

//ПЕРЕПИСАТЬ НА 5 ПОЛЕЙ И КОНКАТЕНАЦИЮ

// 2 2 0 0 off = = = = = = = 2*3 x 2*5
// 1 1 0 0 off = = = = = = = 3 x 5

ApplicationWindow {
    id: window
    visible: true
    width: 800
    height: 800
    title: qsTr("Lab")
    property bool lastMatrix
    property bool allMatrixes
    property bool waitForReady
    property var result
    property int i: 1

    Component.onCompleted: {
        socket.active = true
    }

    header: ToolBar {}

    WebSocket {
        id: socket
        url: "ws://sprs.herokuapp.com/first/liza_volkova"

        function sendText(text) {
            log.text += '\nSENT: ' + text
            socket.sendTextMessage(text)
        }

        property var matrica
        property var clearMatrix
        onMatricaChanged: {
            lastMatrix = false
            console.log('-====================== onMatricaChanged')
            allMatrixes = false
            //console.log('matrica false', matrica)
            result = lab.convertToVector(matrica, lab.possib)
            waitForReady = true
            socket.sendText(i+" "+result)



        }
        property var all
        onAllChanged: {
            lastMatrix = false
            lab.process(all)

            lastMatrix = true
            socket.sendText("Ready")
            // sendInput.text = ''
            // i++



        }


        property bool waitForNextMatrix

        property bool allM
        property bool lastM
        property bool lastMRes

        onTextMessageReceived: {
            log.text += "\nRECIEVED: " + message

            if (allM) {
                console.log(message.length)
                allM = false
                lab.process(message)
                lastM = true
                socket.sendText("Ready")
            }
            else if (lastM) {
                console.log(message.length)
                lastM = false
                result = lab.convertToVector(message, lab.possib)
                lastMRes = true
                socket.sendText(i + " " + result)
                i++
            }
            else if (lastMRes) {
                lastMRes = false
                lastM = true
                socket.sendText("Ready")
            }
//            if (i > lab.steps)
//                socket.sendText('Bye')
                                    /*uncomment this for first time usage*/

            //            if (message.indexOf('Solve') !== -1) {
            //                let result = eval(message.substring(5))
            //                console.log('Send:', result)
            //                sendTextMessage(result)
            //            }

            //            if (message.indexOf('Correct') !== -1) {
            //                socket.active = false
            //                socket.active = true
            //            }


//                if (lastMatrix) {
//                    lastMatrix = false
//                    console.log('message', message)
//                    if (message.length < 20) return
//                    console.log('--------------------', i)
//                    result = lab.convertToVector(message, lab.possib)
//                    socket.sendText(i + " " + result)
//                }
//                else if (allMatrixes) {
//                    allMatrixes = false
//                    lastMatrix = false
//                    lab.process(all)

//                    lastMatrix = true
//                    socket.sendText("Ready")
//                    all = message
//                }
//                else if (waitForReady) {
//                    waitForReady = false
//                    i++
//                    socket.sendText("Ready")
//                } else
//                    waitForReady = true

        }
        onStatusChanged: {
            if (socket.status == WebSocket.Error) {
                log.text += '\nError: ' + socket.errorString
            } else if (socket.status == WebSocket.Open) {
                socket.sendText("Let's start")
            } else if (socket.status == WebSocket.Closed) {
                log.text += "\nSocket closed"
            }
        }

        active: false
    }

    Column {
        id: menu
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.top: parent.top
        anchors.topMargin: 28
        y: 5
        spacing: 20


        Label {
            id: restartB
            anchors.right: parent.right; anchors.rightMargin: 55
            // height: 48; width: 170
            // highlighted: true

            text: {

                switch(socket.status) {
                case WebSocket.Closed:
                case WebSocket.Closing:
                case WebSocket.Error:
                    return 'ERROR'
                case WebSocket.Connecting:
                    return 'CONNECTING...'
                default:
                    return 'OK'
                }
            }

//            onClicked: {
//                socket.active = false
//                socket.active = true
//                log.text = ''
//                lab.clear()
//                lastMatrix = false
//                allMatrixes = false
//                result = undefined

//                socket.matrica = undefined
//                socket.clearMatrix = undefined
//                socket.all = undefined
//                //need to clear the vectors in cpp
//            }
        }
        Pane {
            id: sendZone
            height: 174
            width: 263
            anchors.right: parent.right
            ColumnLayout {
                //    anchors.fill: parent
                anchors.top: parent.top
                anchors.left: parent.left; anchors.right: parent.right

                Label {
                    text: "1."
                    Layout.alignment: Qt.AlignHCenter
                    font.pixelSize: 25
                }


                Label {
                    text: "Input settings:"
                    font.pixelSize: 20

                }

                TextField {
                    id: sendInput
                    //anchors.horizontalCenter: parent.horizontalCenter
                    //text: '6 6 0.7 0 off'
                    text: '6 6 0.8 10 off'
                }


                Button {
                    Layout.topMargin: 5
                    highlighted: true
                    text: 'SEND'
                    onClicked: {
                        socket.allM = true

                        let list = sendInput.text.split(' ')
                        lab.possib = list[2]
                        lab.width = list[0]
                        lab.height = list[1]
                        lab.steps = list[3]
                        socket.sendText(sendInput.text)
                        console.log(lab.width, lab.height, lab.possib)
                        // sendInput.text = ''



                    }

                }
            }

        }


        Pane {
            id: ready
            height: 100
            width: 263
            anchors.right: parent.right
            visible: false
            ColumnLayout {
                //    anchors.fill: parent
                anchors.top: parent.top
                anchors.left: parent.left; anchors.right: parent.right

                Label {
                    text: "2."
                    Layout.alignment: Qt.AlignHCenter
                    font.pixelSize: 25
                }
                Button {
                    text: 'Ready'
                    highlighted: true
                    onClicked: {
                        lastMatrix = true
                        socket.sendText("Ready")
                        // sendInput.text = ''
                        // i++
                    }

                }
            }
        }

        Pane {
            id: sendResult
            height: 174
            width: 263
            anchors.right: parent.right
            visible: false
            ColumnLayout {
                //    anchors.fill: parent
                anchors.top: parent.top
                anchors.left: parent.left; anchors.right: parent.right

                Label {
                    text: "3."
                    Layout.alignment: Qt.AlignHCenter
                    font.pixelSize: 25
                }

                Button {
                    text: 'Send result'
                    font.pixelSize: 20
                    highlighted: true


                    onClicked: {
                        socket.sendText(i+" "+result)
                    }
                }
            }
        }
        Pane {
            id: bye
            height: 174
            width: 263
            anchors.right: parent.right
            visible: false
            ColumnLayout {
                //    anchors.fill: parent
                anchors.top: parent.top
                anchors.left: parent.left; anchors.right: parent.right

                Label {
                    text: "4."
                    Layout.alignment: Qt.AlignHCenter
                    font.pixelSize: 25
                }

                Button {
                    text: 'Bye'
                    font.pixelSize: 18
                    highlighted: true


                    onClicked: {
                        socket.sendText("Bye")
                    }
                }
            }
        }
    }

    Row {
        id: answers

    }

    //    Flickable {
    //        id: flick

    //        anchors.top: logZone.top; anchors.topMargin: 20
    //        anchors.left: parent.left; anchors.leftMargin: 5
    //        anchors.right: parent.right; anchors.rightMargin: 5
    //        anchors.bottom: parent.bottom; anchors.bottomMargin: 5

    //        clip: true

    //        function ensureVisible(r) {
    //            if (contentY >= r.y)
    //                contentY = r.y;
    //            else if (contentY+height <= r.y+r.height)
    //                contentY = r.y+r.height-height;
    //        }

    Pane {
        id: logZone
        height: 720
        anchors.right: menu.left
        anchors.left: parent.left
        anchors.leftMargin: 25
        anchors.top: parent.top
        anchors.topMargin: 28
        //border.color:"#7CB493"
        //border.width: 3
        //radius: 19

        Flickable {
            id: scroll
            anchors.fill: parent
            contentWidth: log.paintedWidth
            contentHeight: log.paintedHeight

            clip: true

            function ensureVisible(r) {
                if (contentX >= r.x)
                    contentX = r.x;
                else if (contentX+width <= r.x+r.width)
                    contentX = r.x+r.width-width;
                if (contentY >= r.y)
                    contentY = r.y;
                else if (contentY+height <= r.y+r.height)
                    contentY = r.y+r.height-height;
            }

            ScrollIndicator.vertical: ScrollIndicator {}

            Text {
                id: log//; selectByMouse: true
                x: 5
                // width: scroll.width
                // wrapMode: Text.Wrap
                // onCursorRectangleChanged: scroll.ensureVisible(cursorRectangle)
                font.pixelSize: 10
                color: 'white'
                onTextChanged: if (log.height > scroll.height) scroll.contentY = log.height - scroll.height
            }
        }
    }

    Lab {
        id: lab
    }
}
